## todo
- [x] mutimedia framework
- [x] sidebar. select node and draw vector thumbnail
- [x] canvas "clear" button
- [x] term icon preview
- [x] term info window
- [x] movable component
- [ ] save to file
- [ ] component picker window
- [ ] "add term" button
- [ ] selectable component
- [ ] drag from sidebar to add node
- [ ] click node to select. press backspace/delete key to delete
- [ ] "reset layout" button (clip all nodes inside screen, reset ports positions to default)

design: positions x/y are in [0, 1) (scale with window size)
