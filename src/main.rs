mod logic;

use logic::Vec2;
use logic::*;

use ggez::event::MouseButton;
use ggez::graphics::{self as gfx, Color, FillOptions, Mesh, MeshBuilder, StrokeOptions};
use ggez::{conf::WindowMode, event::EventHandler, GameError};
use ggez_egui::{egui, EguiBackend};
use egui::FontDefinitions;
use glam::Vec2 as mVec2;
const MB_PRIMARY: MouseButton = MouseButton::Left;

pub struct MainState {
    pub doc: Document,
    pub font_noto: &'static str,
    pub window_size: Vec2,
    pub egui_backend: EguiBackend,
    pub show_canvas: bool,
    pub canvas_drawing_started: bool,
    pub thumbs: Vec<(TermId, gfx::Rect)>,
    pub dragging: Option<String>,
    // pub font_m5x7: Font,
}

impl MainState {
    fn new(ctx: &mut ggez::Context) -> Result<Self, GameError> {
        let doc = example_document();

        let font_noto = "Noto Sans";
        ctx.gfx.add_font(
            font_noto,
            ggez::graphics::FontData::from_path(ctx, "/NotoSans-Regular.ttf")?,
        );

        let (w, h) = ctx.gfx.drawable_size();

        Ok(Self {
            doc,
            egui_backend: generate_egui_backend(ctx),
            font_noto,
            window_size: [w as f32, h as f32].into(),
            canvas_drawing_started: false,
            show_canvas: true,
            thumbs: vec![],
            dragging: None,
        })
    }

    fn try_drag_component(&mut self, ctx: &mut ggez::Context) {
        let Some(el) = self.doc.term_edited_mut() else { return };
        let point = ctx.mouse.position();
        let button_just_pressed = ctx.mouse.button_just_pressed(MB_PRIMARY);
        for comp in &mut el.components {
            match &self.dragging {
                Some(id) if id == comp.id() => {
                    comp.move_by(ctx.mouse.delta().into());
                }
                _ => {}
            }

            // }
            if button_just_pressed && comp.pointer_over(point.into()) {
                self.dragging = Some(comp.id().to_owned());
                break;
            }
        }

        if !ctx.mouse.button_pressed(MB_PRIMARY) {
            self.dragging = None;
        }
    }
}

type DynError = Box<dyn std::error::Error>;
pub fn main() -> Result<(), DynError> {
    let w = 800.;
    let h = 600.;

    let cb = ggez::ContextBuilder::new("helloworld", "ggez")
        .window_mode(WindowMode::default().dimensions(w, h))
        .add_resource_path("assets");
    let (mut ctx, event_loop) = cb.build()?;

    let state = MainState::new(&mut ctx)?;
    ggez::event::run(ctx, event_loop, state);
    // Ok(())
}

const EGUI_SCALE_FACTOR: f32 = 1.2;

fn generate_egui_backend(ctx: &ggez::Context) -> EguiBackend {
    let mut backend = EguiBackend::new(ctx);
    let (w, h) = ctx.gfx.drawable_size();
    backend.input.set_scale_factor(EGUI_SCALE_FACTOR, (w, h));
    let ctx = backend.ctx();
    ctx.set_fonts({
        let mut font_defs = FontDefinitions::default();

        font_defs.font_data.insert(
            "NotoSans".to_owned(),
            egui::FontData::from_static(include_bytes!("../assets/NotoSans-SemiBold.ttf")),
        );

        font_defs
            .families
            .get_mut(&egui::FontFamily::Proportional)
            .unwrap()
            .insert(0, "NotoSans".into());
        font_defs
    });
    backend
}

impl EventHandler for MainState {
    fn update(&mut self, ctx: &mut ggez::Context) -> Result<(), GameError> {
        self.window_size = {
            let (w, h) = ctx.gfx.size();
            [w, h].into()
        };
        let egui_ctx = self.egui_backend.ctx();
        egui::Window::new("info").show(&egui_ctx, |ui| {
            self.main_window(ui);
        });

        egui::Window::new("term info").show(&egui_ctx, |ui| {
            if let Some(el) = self.doc.term_edited_mut() {
                let el = el.to_owned(); // ineffecient copy to beat borrow checker
                let el_next = self.term_info_window(ui, el);
                if let Some(el_ref) = self.doc.term_edited_mut() {
                    *el_ref = el_next;
                }
            } else {
                ui.label("No term selected to edit");
            }
        });
        self.egui_backend.update(ctx);

        if !egui_ctx.is_using_pointer() {
            self.try_drag_component(ctx);
            if self.show_canvas {
                self.update_canvas(ctx);
            }
        }

        Ok(())
    }

    fn draw(&mut self, ctx: &mut ggez::Context) -> Result<(), GameError> {
        let mut canvas = gfx::Canvas::from_frame(ctx, gfx::Color::from([0., 0.1, 0.1, 1.0]));
        let doc = &self.doc;

        if let Some(el) = doc.term_edited() {
            if self.show_canvas {
                let editable_canvas_aabb = self.canvas_aabb();
                draw_canvas(ctx, &mut canvas, editable_canvas_aabb, &el.icon)?;
            }

            for port in el.inputs() {
                self.draw_port(ctx, &mut canvas, port)?;
            }

            for port in el.outputs() {
                self.draw_port(ctx, &mut canvas, port)?;
            }
        }

        canvas.draw(&self.egui_backend, [0.0, 0.0]);
        for (id, rect) in &self.thumbs {
            if let Some(term) = self.doc.named_terms.iter().find(|doc| &doc.id == id) {
                draw_canvas(ctx, &mut canvas, *rect, &term.icon)?;
            }
        }

        canvas.finish(ctx)?;

        Ok(())
    }
}

impl MainState {
    fn main_window(&mut self, ui: &mut egui::Ui) {
        let doc = &mut self.doc;
        self.thumbs.clear();
        let ppx = ui.ctx().pixels_per_point();
        for term in doc.named_terms.iter() {
            ui.horizontal(|ui| {
                if ui
                    .selectable_label(doc.editing == term.id, &term.name)
                    .clicked()
                {
                    doc.editing = term.id.clone();
                }
                let rect = ui
                    .allocate_response([32., 32.].into(), egui::Sense::hover())
                    .rect;
                let rect = gfx::Rect::new(
                    rect.left() * ppx,
                    rect.top() * ppx,
                    rect.width() * ppx,
                    rect.height() * ppx,
                );
                self.thumbs.push((term.id.clone(), rect));
            });
        }
    }

    fn term_info_window(&mut self, ui: &mut egui::Ui, mut el: NamedTerm) -> NamedTerm {
        ui.label(&format!("{0}", el.name));
        ui.label(&format!("id: {0}", el.id));
        ui.group(|ui| {
            ui.label("Inputs");
            el.inputs().for_each(|port| {
                if ui
                    .selectable_label(false, &format!("{}: {}", port.name, port.typeinfo))
                    .clicked()
                {
                    todo!();
                }
            });
        });
        ui.group(|ui| {
            ui.label("Outputs");
            el.outputs().for_each(|port| {
                if ui
                    .selectable_label(false, &format!("{}: {}", port.name, port.typeinfo))
                    .clicked()
                {
                    todo!();
                }
            });
        });
        ui.group(|ui| {
            ui.label("Components");
            el.components.iter().for_each(|comp| {
                if ui.selectable_label(false, &format!("{comp:?}")).clicked() {
                    todo!();
                }
            });
        });
        ui.checkbox(&mut self.show_canvas, "Show Canvas");
        if self.show_canvas {
            if ui.button("clear canvas").clicked() {
                el.icon = Default::default();
            }
        }
        el
    }

    fn canvas_aabb(&self) -> gfx::Rect {
        #[allow(non_snake_case)]
        let CANVAS_SIZE: Vec2 = [400., 400.].into();

        gfx::Rect::new(
            0.,
            self.window_size.y - CANVAS_SIZE.y,
            CANVAS_SIZE.x,
            CANVAS_SIZE.y,
        )
    }

    fn draw_text(
        &self,
        canvas: &mut gfx::Canvas,
        text: &str,
        position: impl Into<Vec2>,
        text_size: f32,
    ) {
        canvas.draw(
            gfx::Text::new(text)
                .set_font(self.font_noto)
                .set_scale(text_size),
            position.into(),
        );
    }

    fn draw_port(
        &self,
        ctx: &mut ggez::Context,
        canvas: &mut gfx::Canvas,
        port: &Port,
    ) -> Result<(), GameError> {
        let aabb = port.aabb();
        let rect = Mesh::new_rectangle(
            ctx,
            gfx::DrawMode::Stroke(StrokeOptions::default().with_line_width(1.)),
            aabb,
            Color::WHITE,
        )?;
        canvas.draw(&rect, [0., 0.]);

        let text_pos = port.top_left() + into_mvec2([0.0, -20.0]);
        self.draw_text(canvas, &port.name, text_pos, 16.);
        Ok(())
    }

    fn update_canvas(&mut self, ctx: &mut ggez::Context) {
        let aabb = self.canvas_aabb();
        let Some(el) = self.doc.term_edited_mut() else { return };
        let screen_point: mVec2 = ctx.mouse.position().into();
        let mut normalized_point =
            (screen_point - into_mvec2(aabb.point())) / into_mvec2(aabb.size());
        let inside_canvas = {
            let p = normalized_point;
            p.x >= 0. && p.x <= 1. && p.y >= 0. && p.y <= 1.
        };
        if inside_canvas && ctx.mouse.button_just_pressed(MB_PRIMARY) {
            el.icon.paths.push(Default::default());
            self.canvas_drawing_started = true;
        }
        if self.canvas_drawing_started {
            clip(&mut normalized_point.x, 0., 1.);
            clip(&mut normalized_point.y, 0., 1.);
            if let Some(last_path) = el.icon.paths.last_mut() {
                let close_enough = match last_path.last() {
                    Some(&last_point) => {
                        (into_mvec2(last_point) - normalized_point).length() < 0.01
                    }
                    None => false,
                };
                if !close_enough {
                    last_path.push(normalized_point.into());
                }
            }
        }
        if !ctx.mouse.button_pressed(MB_PRIMARY) {
            self.canvas_drawing_started = false;
        }
    }
}

fn into_mvec2(point: impl Into<mVec2>) -> mVec2 {
    point.into()
}

fn draw_canvas(
    ctx: &mut ggez::Context,
    canvas: &mut gfx::Canvas,
    aabb: gfx::Rect,
    thumbnail: &VectorPicture,
) -> Result<(), GameError> {
    let bg = Mesh::new_rectangle(
        ctx,
        gfx::DrawMode::Fill(FillOptions::non_zero()),
        aabb,
        gfx::Color::BLACK,
    )?;
    canvas.draw(&bg, [0., 0.]);

    let mut lines = MeshBuilder::new();

    for path in thumbnail.paths.iter() {
        let mut points: Vec<_> = path
            .into_iter()
            .map(|&p| {
                let p: mVec2 = p.into();
                let aabb_size: mVec2 = aabb.size().into();
                let top_left: mVec2 = aabb.point().into();
                p * aabb_size + top_left
            })
            .collect();
        if points.len() == 0 {
            continue;
        }
        if points.len() == 1 {
            points.push(points[0]);
        }
        lines.polyline(
            gfx::DrawMode::Stroke(StrokeOptions::default().with_line_width(1.)),
            &points,
            Color::WHITE,
        )?;
    }
    canvas.draw(&Mesh::from_data(ctx, lines.build()), [0., 0.]);

    Ok(())
}

fn clip(x: &mut f32, lower: f32, upper: f32) {
    *x = x.max(lower).min(upper);
}
