use ggez::graphics::Rect;
use nanoid::nanoid;
use serde::{Deserialize, Serialize};

pub type Vec2 = glam::Vec2;

pub type TypeInfo = String;

// stub
pub type TermId = String;
pub type LinkId = String;
pub type PortId = String;

#[derive(Clone, Serialize, Deserialize, Debug)]
pub enum PortType {
    Data,
    Effect,
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub enum PortDirection {
    Input,
    Output,
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct Port {
    pub id: PortId,
    pub r#type: PortType,
    pub direction: PortDirection,
    pub name: String,
    pub typeinfo: TypeInfo, // maybe ports can be used as effect port as well?
    pub aux_pos: Vec2,
}

const PORT_SIZE: Vec2 = Vec2::new(40., 40.);

impl Port {
    pub fn aabb(&self) -> Rect {
        let top_left = self.top_left();
        Rect::new(top_left.x, top_left.y, PORT_SIZE.x, PORT_SIZE.y)
    }

    pub fn top_left(&self) -> Vec2 {
        let port_center = self.aux_pos;
        port_center - PORT_SIZE / 2.
    }
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct InstancedTerm {
    pub instance_id: TermId,
    pub template_id: TermId,
    pub aux_pos: Vec2,
}

const TERM_SIZE: Vec2 = Vec2::new(32., 32.);
impl InstancedTerm {
    pub fn aabb(&self) -> Rect {
        let top_left = self.top_left();
        Rect::new(top_left.x, top_left.y, TERM_SIZE.x, TERM_SIZE.y)
    }

    pub fn top_left(&self) -> Vec2 {
        let port_center = self.aux_pos;
        port_center - TERM_SIZE / 2.
    }
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub enum LinkType {
    Data,
    Effects,
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct Link {
    id: LinkId,
    instance_id_from: TermId,
    instance_id_to: TermId,
    r#type: LinkType,
}
impl Link {
    pub fn line(&self) -> (Vec2, Vec2) {
        todo!()
    }
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub enum Component {
    Term(InstancedTerm),
    Link(Link),
    Port(Port),
}

pub struct ComponentSense {
    /// this component can be moved around
    pub drag: bool,
    /// this component can be selected
    pub focusable: bool,
}

impl Component {
    pub fn id(&self) -> &str {
        match self {
            Component::Term(x) => &x.instance_id,
            Component::Link(x) => &x.id,
            Component::Port(x) => &x.id,
        }
    }
    pub fn sense(&self) -> ComponentSense {
        match self {
            Component::Term(_) => ComponentSense {
                drag: true,
                focusable: true,
            },
            Component::Link(_) => ComponentSense {
                drag: false,
                focusable: true,
            },
            Component::Port(_) => ComponentSense {
                drag: true,
                focusable: true,
            },
        }
    }

    pub fn pointer_over(&self, point: Vec2) -> bool {
        match self {
            Component::Term(term) => term.aabb().contains(point),
            Component::Link(link) => point_over_line_within_threshold(point, link.line(), 2.),
            Component::Port(port) => port.aabb().contains(point),
        }
    }

    pub fn move_by(&mut self, delta: Vec2) {
        match self {
            Component::Term(InstancedTerm { aux_pos, .. }) => {
                *aux_pos += delta;
            }
            Component::Port(Port { aux_pos, .. }) => {
                *aux_pos += delta;
            }
            _ => unreachable!(),
        }
    }
}

// todo: verify this works
fn point_over_line_within_threshold(
    point: glam::Vec2,
    line: (glam::Vec2, glam::Vec2),
    error: f32,
) -> bool {
    // affine transform line to 0,0 to 1,0.
    let mut angle = (line.1 - line.0).normalize();
    angle.y = -angle.y;
    
    let rotated_point = (point - line.0).rotate(angle);

    rotated_point.x >= 0. - error
        && rotated_point.x <= 1. + error
        && rotated_point.x >= -error
        && rotated_point.x <= error
}

#[derive(Clone, Serialize, Deserialize, Default)]
pub struct VectorPicture {
    pub paths: Vec<Vec<Vec2>>,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct NamedTerm {
    pub id: TermId,
    /// display name
    pub name: String,
    pub icon: VectorPicture,
    /// other terms used
    pub components: Vec<Component>,
}
impl NamedTerm {
    pub fn inputs(&self) -> impl Iterator<Item = &Port> {
        self.components.iter().filter_map(|comp| match comp {
            Component::Port(
                port @ Port {
                    direction: PortDirection::Input,
                    ..
                },
            ) => Some(port),
            _ => None,
        })
    }

    pub fn outputs(&self) -> impl Iterator<Item = &Port> {
        self.components.iter().filter_map(|comp| match comp {
            Component::Port(
                port @ Port {
                    direction: PortDirection::Output,
                    ..
                },
            ) => Some(port),
            _ => None,
        })
    }
}

#[derive(Clone, Serialize, Deserialize)]
pub struct Document {
    pub named_terms: Vec<NamedTerm>,
    pub editing: TermId,
}
impl Document {
    pub fn term_edited(&self) -> Option<&NamedTerm> {
        self.named_terms.iter().find(|t| t.id == self.editing)
    }

    pub fn term_edited_mut(&mut self) -> Option<&mut NamedTerm> {
        self.named_terms.iter_mut().find(|t| t.id == self.editing)
    }
}

pub fn example_document() -> Document {
    let term0 = new_example_term("term_0");
    let editing = term0.id.clone();
    let term1 = new_example_term("term_1");
    Document {
        named_terms: vec![term0, term1],
        editing,
    }
}

fn new_example_term(name: &str) -> NamedTerm {
    NamedTerm {
        id: nanoid!(),
        name: name.into(),
        icon: Default::default(),
        components: vec![
            Component::Port(Port {
                id: nanoid!(),
                name: "input_0".into(),
                typeinfo: "Nat".into(),
                r#type: PortType::Data,
                direction: PortDirection::Input,
                aux_pos: calc_input_port_pos(0),
            }),
            Component::Port(Port {
                id: nanoid!(),
                name: "output_0".into(),
                typeinfo: "Nat".into(),
                r#type: PortType::Data,
                direction: PortDirection::Output,
                aux_pos: calc_output_port_pos(0),
            }),
        ],
    }
}

fn calc_input_port_pos(i: usize) -> Vec2 {
    ([300.0, 100.0 + 100.0 * i as f32]).into()
}

fn calc_output_port_pos(i: usize) -> Vec2 {
    ([500.0, 100.0 + 100.0 * i as f32]).into()
}
